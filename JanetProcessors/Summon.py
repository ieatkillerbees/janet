from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import messages_only, startswith_words
from events.Message import Message
import requests
import re
import lxml.html
import json

class Summon(JanetProcessor):
    @startswith_words("?summon")
    def process(self):
        self.reply(self.summoned_request)

    @property
    def summoned_request(self):
        if not self.summon_target:
            return self.random_polite_refusal
        return self.google_image_search(self.summon_target)

    def google_image_search(self, query):
        # Super duper ripped off from Etsy's irccat
        headers = {}
        headers["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36" 
        params = {
            'tbm': "isch",
            'q': query,
            'safe': "strict"
        }
        search_url = "https://www.google.com/search"
        response = requests.get(search_url, params=params, headers=headers)
        tree = lxml.html.fromstring(response.text)
        image_elems = tree.cssselect('div.rg_meta')
        first_result = image_elems[0]
        first_result = json.loads(first_result.text_content())
        return first_result["ou"]


    @property
    def summon_target(self):
        if self.split_text and len(self.split_text) > 1:
            return " ".join(self.split_text[1:])

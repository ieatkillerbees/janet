from JanetProcessors.JanetProcessor import JanetProcessor
from .decorators import entry_point, messages_only, allow_events
from events.Message import Message
import gitlab
import settings

class GitlabIssue(JanetProcessor):
    janet_proj_id = 8963809
    gl = gitlab.Gitlab('https://gitlab.com', private_token=settings.github_access_token)

    @messages_only()
    def process(self):
        if self.event.text.startswith("?issues"):
            for issue in self.issues:
                self.reply(issue.web_url)
        elif self.event.text.startswith("?issue "):
            self.create_issue()

    def create_issue(self):
        issue_title = " ".join(self.clean_args)
        issue_args = {}
        issue_args['title'] = issue_title
        issue_args['description'] = "Empty for testing"

        new_issue = self.project.issues.create(issue_args)
        self.reply(new_issue.web_url)

    @property
    def project(self):
        return self.gl.projects.get(self.janet_proj_id)

    @property
    def issues(self):
        return self.project.issues.list()

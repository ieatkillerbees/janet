## Don't let the imports scare you
## There's just a lot of utility libraries in here

## Our custom janet-y stuff
from .JanetProcessor import JanetProcessor
from .decorators import startswith_words, allow_events, no_bots, contains_words, i_contains_words

## Event types
from events.Message import Message
from events.ReactionAdded import ReactionAdded
from events.Hello import Hello

## Python stdlib
import time
import importlib
import requests
import gevent
import os

class Time(JanetProcessor):
    @startswith_words("?time")
    def process(self):
        self.reply(time.time())

class Hi(JanetProcessor):
    @allow_events(Hello)
    def process(self):
        self.send_message(self.version)

class Echo(JanetProcessor):
    @startswith_words("?echo")
    def process(self):
        text = self.event.text
        message = text.partition(' ')[2]
        self.reply(message)

class Sleep(JanetProcessor):
    @startswith_words("?sleep")
    @no_bots()
    def process(self):
        text = self.event.text
        duration = text.partition(' ')[2]
        try:
            duration = float(duration)
        except ValueError:
            self.reply(self.random_polite_refusal)
            return
        self.reply(f"Putting worker to sleep for {duration}sec")
        gevent.sleep(duration)
        self.reply("Waking up worker")
        return duration

class Ping(JanetProcessor):
    @i_contains_words("ping")
    def process(self):
        self.reply("pong")

class Host(JanetProcessor):
    @startswith_words("?host")
    def process(self):
        self.reply(os.uname()[1])

class Version(JanetProcessor):
    @startswith_words("?vers")
    def process(self):
        self.reply(self.version)

class Processors(JanetProcessor):
    @startswith_words("?processors")
    def process(self):
        processors = importlib.import_module("JanetProcessors")
        for processor in processors:
            self.reply(processor)


class Curl(JanetProcessor):
    @property
    def request_url(self):
        if len(self.split_text) > 1:
            return self.split_text[1]

    @startswith_words("?curl")
    def process(self):
            self.reply(requests.get(self.request_url))

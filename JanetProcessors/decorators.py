from events.Message import Message
from events.ReactionAdded import ReactionAdded

# All this decorator does is add the JANET_ENTRY
# attribute to the function object. 
# This lets us loop over all the attributes 
# of the processor and find ones with a JANET_ENTRY
class basic_func_wrapper():
    JANET_ENTRY = True
    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def __call__(self, processor):
        return self.func(processor)

    def __repr__(self):
        return self.func.__repr__()


class no_bot_func_wrapper(basic_func_wrapper):
    def __call__(self, processor):
        if not processor.event.sender.is_bot:
            return self.func(processor)

def typed_func_wrapper(*accepted_types):
    a_types = accepted_types # sometimes I don't understand scope

    class inner_wrapper(basic_func_wrapper):
        accepted_types = a_types

        def __call__(self, processor):
            if type(processor.event) in accepted_types:
                return self.func(processor)

    return inner_wrapper

def startswith_words(*words):
    w = words 

    class inner_wrapper(basic_func_wrapper):
        words = tuple(w)

        def __call__(self, processor):
            if not type(processor.event) == Message: 
                return
            if processor.event.text.startswith(self.words):
                return self.func(processor)

    return inner_wrapper

def i_startswith_words(*words):
    w = [word.lower() for word in words]

    class inner_wrapper(basic_func_wrapper):
        words = tuple(w)

        def __call__(self, processor):
            if not type(processor.event) == Message: 
                return
            if processor.event.text.lower().startswith(self.words):
                return self.func(processor)

    return inner_wrapper

def contains_words(*words):
    w = words 

    class inner_wrapper(basic_func_wrapper):
        words = tuple(w)

        def __call__(self, processor):
            if not type(processor.event) == Message: 
                return

            message_text = processor.event.text
            if any([word in message_text for word in self.words]):
                return self.func(processor)

    return inner_wrapper

def i_contains_words(*words):
    w = [word.lower() for word in words]

    class inner_wrapper(basic_func_wrapper):
        words = tuple(w)

        def __call__(self, processor):
            # I wanna inherit from typed_func_wrapper or something
            # so I don't have to do write out the type check 
            if not type(processor.event) == Message: 
                return

            message_text = processor.event.text.lower()
            if any([word in message_text for word in self.words]):
                return self.func(processor)

    return inner_wrapper

class entry_point():
    func_wrapper = basic_func_wrapper
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __call__(self, processor_class):
        return self.func_wrapper(processor_class)

class messages_only(entry_point):
    func_wrapper = typed_func_wrapper(Message)

class reacts_only(entry_point):
    func_wrapper = typed_func_wrapper(ReactionAdded)

class allow_events(entry_point):
    def __init__(self, *args, **kwargs):
        self.func_wrapper = typed_func_wrapper(*args)
        super().__init__(*args, **kwargs)

class no_bots(entry_point):
    func_wrapper = no_bot_func_wrapper

from JanetProcessors.JanetProcessor import JanetProcessor
from .decorators import i_contains_words
from events.Message import Message

class ILoveYou(JanetProcessor):
    @i_contains_words("I love you")
    def process(self):
        self.send_message("https://imgur.com/a/WGV9niN")


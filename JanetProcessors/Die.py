from JanetProcessors.JanetProcessor import JanetProcessor
from .decorators import messages_only, no_bots, startswith_words
from events.Message import Message
import sys

class Learn(JanetProcessor):
    exit_triggers = ("?die", "?exit", "?restart", "?reboot")
    
    @startswith_words(*exit_triggers)
    def process(self):
        self.reply("Ohhhh oh no no no please don't")
        sys.exit()

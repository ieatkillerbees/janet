# Real experimental bullshit meta-programming in here
# I'm just doing this to get like...
# import JanetProcessors
# for janet in JanetProcessors:
#  janet(event)
# which is some wild-ass syntax that I am here for
import types
import sys
import os
import importlib
import inspect
from events.GenericEvent import GenericEvent
from .JanetProcessor import JanetProcessor

class JanetProcessorLoader(types.ModuleType):
    def __init__(self, *args, **kwargs):
        super().__init__(name="JanetProcessors")
        self.modules = []
        
        for potential_module in os.listdir(os.path.dirname(__file__)):
            current_directory = os.path.dirname(__file__)
            # Maybe it would be better to just *try* importing the name
            # this is duplicating behavior of the stdlib import
            # in an unpredictable way
            blocklist = ["__init__.py", "__pycache__", "JanetProcessor.py"]
            if potential_module in blocklist:
                continue

            if not self.name_is_module(potential_module):
                continue

            module_name = self.name_to_modulename(potential_module)
            self.modules.append(importlib.import_module(module_name))

    def name_to_modulename(self, path):
        file_name, file_extension = os.path.splitext(path)
        file_name = os.path.basename(file_name)
        module_name = f"JanetProcessors.{file_name}"
        return module_name

    def name_is_module(self, name):
        module_name = self.name_to_modulename(name)
        try:
            importlib.import_module(module_name)
            return True
        except ModuleNotFoundError:
            print("FATAL: couldn't load {}. Aborting startup!")
            raise

    @property
    def processor_classes(self):
        if not hasattr(self, "_processors"):
            self._processors = []
            all_classes_in_module = [inspect.getmembers(module, inspect.isclass) for module in self.modules]
            all_classes_in_module = [item for sublist in all_classes_in_module for item in sublist] #flatten list
            all_processors = [processor_class[1] for processor_class in all_classes_in_module if issubclass(processor_class[1], JanetProcessor)]
            active_processors = [processor for processor in all_processors if hasattr(processor(GenericEvent({"type": "bogus"})), "entry_points")]
            self._processors = active_processors
        return self._processors

    def __iter__(self):
        return iter(self.processor_classes)

    def __path__(self):
    	return [os.path.dirname(os.path.abspath(__file__))]

sys.modules[__name__] = JanetProcessorLoader() 

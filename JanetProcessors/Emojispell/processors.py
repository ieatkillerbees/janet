from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import i_startswith_words
from events.Message import Message
from .mapper import Mapper

from models.User import User

class Emojispell(JanetProcessor):
    emojispell_words = ("?emojispell", "?ransomspell", "?ransomcase")

    @i_startswith_words(*emojispell_words)
    def process(self):
        if self.event.text.startswith(self.emojispell_words):
            plaintext = " ".join(self.clean_args)
            trans_text_lol = Mapper(plaintext)

            self.reply(trans_text_lol)
            return trans_text_lol

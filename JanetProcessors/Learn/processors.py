from  sqlalchemy.sql.expression import func, select
from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import messages_only, no_bots
from events.Message import Message
from sqlalchemy import or_
from . import models
import uuid

from models.User import User

class Learn(JanetProcessor):
    get_learns_words = ("?learn?", "gimme")

    @messages_only()
    @no_bots()
    def listen(self):
        if self.event.text.startswith("?learn "):
            self.learn()
        elif self.event.text.startswith(self.get_learns_words):
            self.recall()
        elif self.event.text.startswith("?"):
            self.recall()

    def learn(self):
        if not (self.learn_target and self.learn_text):
            self.reply(self.random_polite_refusal)
            return

        session = self.new_session()
        new_learn = models.Learn(target=self.learn_target, text=self.learn_text)
        session.add(new_learn)
        self.reply(f"OK, learned {self.learn_target}")
        session.commit()
        session.close()

    def recall(self):
        session = self.new_session()
        recalled_learn = session.query(models.Learn).filter(models.Learn.target == self.learn_target).order_by(func.random()).first()
        if recalled_learn:
            self.reply(recalled_learn.text)
        session.close()

    @property
    def learn_target(self):
        if len(self.split_text) >= 2:
            candidate = self.split_text[1]
        elif len(self.split_text):
            candidate = self.split_text[0][1:] # trim off the question mark
        candidate = candidate.lower()

        session = self.new_session()
        maybe_uid = User.normalize_uid(candidate)
        potential_user = session.query(User).filter(or_(User._display_name == candidate, User.uid == maybe_uid)).one_or_none()
        if potential_user:
            candidate = potential_user.at_name

        session.close()
        return candidate

    @property
    def learn_text(self):
        if len(self.split_text) >= 3:
            return " ".join(self.split_text[2:])

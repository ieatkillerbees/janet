from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import i_startswith_words
import random

class Spongecase(JanetProcessor):
    spongecase_words = ("?spongecase",)

    @i_startswith_words(*spongecase_words)
    def process(self):
        if not self.clean_args:
            return

        plain_text = " ".join(self.clean_args)
        spongecased_text = "".join([random.choice([letter.upper, letter.lower])() for letter in plain_text])
        self.reply(spongecased_text)
        return spongecased_text
    

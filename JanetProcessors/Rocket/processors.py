from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import messages_only, i_startswith_words
from events.Message import Message
import os, random

from models.User import User

class Rocket(JanetProcessor):
    rocket_words = ("?rocket")
    # it's just ./rockets but for ~python~
    rockets_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "./rockets")

    @i_startswith_words(rocket_words)
    def process(self):
        rocket_file = os.path.join(self.rockets_path, random.choice(os.listdir(self.rockets_path)))
        rocket = open(rocket_file).read()

        self.reply(rocket)
        return rocket

# None of these are version-pegged
# This is an intentional decision
# It might be a bad one.

pip

slackclient
gevent
python-dateutil
colored
x256
sentry-sdk
sqlalchemy >= 1.3.0b1
lxml
cssselect
boto3
PyMySQL
python-gitlab
sphinx

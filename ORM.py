from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from settings import db_connect_string, worker_count

connect_kwargs = {}
if 'mysql' in db_connect_string:
    connect_kwargs['pool_size'] = 0
    connect_kwargs['max_overflow'] = 1000
    connect_kwargs['pool_pre_ping'] = True
    connect_kwargs['pool_use_lifo'] = True

central_engine = create_engine(f"{db_connect_string}", **connect_kwargs)
session_factory = sessionmaker(bind=central_engine)

# Use this code snippet in your app.
# If you need more information about configurations or implementing the sample code, visit the AWS docs:   
# https://aws.amazon.com/developers/getting-started/python/

import boto3
from botocore.exceptions import ClientError
import json
import base64
import os

def get_secret():

    secret_name = "DEREK_SECRETS"
    region_name = "us-east-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    # We rethrow the exception by default.

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            secret = base64.b64decode(get_secret_value_response['SecretBinary'])
        return secret

secrets = json.loads(get_secret())

slack_tokens = {}
slack_tokens['bot'] = secrets["JANET_XOXB"]
slack_tokens['user'] = secrets["JANET_XOXP"]
github_access_token = secrets["JANET_GHAT"]

mysql_password = secrets['MYSQL_PASSWORD']
db_connect_string = f"mysql+pymysql://janet:{mysql_password}@janets.cluster-cj13n8rbwsbv.us-east-1.rds.amazonaws.com:3306/derek"
worker_count = 100

# You need some delay so that the greenlets have a chance to process
mainloop_delay = .05

SENTRY_ENV = "aws"
os.environ['SENTRY_DSN'] = secrets["SENTRY_DSN"]
# You should be fine if you just set your SENTRY_DSN env var

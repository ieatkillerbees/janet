import requests
from requests.exceptions import HTTPError, ConnectionError
from botocore.exceptions import NoCredentialsError
import os
import time

on_aws = False
no_secrets_available = False

try:
    aws_local_response = requests.get("http://169.254.170.2/v2/metadata", timeout=1)
    on_aws = aws_local_response.ok
except (ConnectionError, HTTPError):
    pass

try:
    from .settings_aws import *
except (NoCredentialsError) as e:
    no_secrets_available = True 

if not on_aws or no_secrets_available:
    from .settings_env import *
    if all(slack_tokens):
        print("SECRETS FROM ENV")
else:
    print("SECRETS FROM AWS")

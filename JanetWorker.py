import gevent
import inspect
import pprint
import time
import sentry_sdk

from JanetNeuron import JanetNeuron
import JanetProcessors
from events.cast_table import cast_table
from events.GenericEvent import GenericEvent
import settings

# JanetWorkers are persistent workers with all the functionality
# of a neuron plus dedicated functionality for processing events.
# By default all queue processing janets are basic JanetWorkers
class JanetWorker(JanetNeuron):
    _event = None

    # just so's you know we also have some @properties in here

    def __init__(self, queue):
        sentry_sdk.init(environment=settings.SENTRY_ENV)
        self.q = queue
        self._processors = []

    # Worker Janets are callable. Functions are first-class citizens after all!
    # Each worker neuron is initialized with a queue and calling the instance e.g. janet_worker()
    # throws it into an infinte loop where it grabs and processes events as fast as possible
    def __call__(self):
        while True:
            # raw_line is where everything else comes from. 
            # If you change the raw_line you change the state of the entire worker.
            self.raw_line = self.q.get()

            #before = time.time()
            self.process_event()
            #processing_time = round(time.time()-before, 3)
            #print(processing_time)

            self.q.task_done()

    # keep 'em freeeeesssshhh
    @property
    def processors(self):
        if not self._processors:
            for processor_class in JanetProcessors:
                processor = processor_class(self.event)
                self._processors.append(processor)
        else:
            for processor in self._processors:
                processor.event = self.event
        return self._processors

    # there's a little magic in here
    # do not look directly at the janet
    def process_event(self):
        print(self.event) # This is where we print the event log to the console
        for processor in self.processors:
            for entry_point in processor.entry_points:
                # you have to call the processing function with the instance as the first 
                # argument because it's become an unbound function. We're just manually adding in `self`
                entry_point(processor)

    @property
    def event(self):
        if self._event and self.raw_line == self._event.raw_line:
            pass
        else:
            type_name = self.raw_line['type']
            if type_name in cast_table:
                self._event = cast_table[type_name](self.raw_line)
            else:
                self._event = GenericEvent(self.raw_line)

        return self._event
